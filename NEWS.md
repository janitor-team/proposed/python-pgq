# NEWS

## pgq 3.5

* Enable Github actions
* Drop Py2 support.
* Upgrade tox setup.

